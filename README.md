# Test Cargo Registry

This repository implements a "proof-of-concept" cargo registry that's hosted entirely within a gitlab project.

The project's git repo is used to store the registry index, and the generic package repo is used to host the crate files.

This type of registry can be thought of as a "personal registry".  

## Can this be used as a "private registry" ?

Not really.  Sort of... maybe?  At the moment, cargo doesn't support authentication when downloading crate files, so the
gitlab project must be configured to allow public/anonymous read-only access.  But if you have a private gitlab instance
that can only be accessed on a private network, then perhaps this approach could be used.

## How to I use crates from this registry?

In your `.cargo/config.toml` file, add this:

```toml
[registries]
test-cargo-registry = {index = "https://gitlab.com/eminence/test-cargo-registry.git"}
```

And then in your `Cargo.toml` file, you can use crates from this registry, like this:

```toml
[dependencies]
another-test-lib = {version = "0.1.0", registry = "test-cargo-registry"}
```

For more info, see the [Using an Alternate Registry](https://doc.rust-lang.org/cargo/reference/registries.html#using-an-alternate-registry)
section of the offical cargo docs.

## How to I publish crates to this registry?

This  registry doesn't implement the web API that lets you use the `cargo publish` command.  Instead, you need to manually update the
index files and manually update the package repository.
